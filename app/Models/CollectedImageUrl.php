<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectedImageUrl extends Model
{
    protected $guarded = [];
}
