<?php

namespace App\Console\Commands;

use App\Models\CollectedImageUrl;
use App\Models\ProcessedLog;
use Illuminate\Console\Command;
use Carbon\Carbon;

class ProcessLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses bot\'s logs and extracts image urls';

    protected $logPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->logPath = config('config.log_path');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->processLogFiles(scandir($this->logPath));

        $this->saveProcessedDate();
    }

    protected function saveProcessedDate()
    {
        $files = scandir($this->logPath);

        $lastLogDate = $this->getLastLogDate($files);

        $processedDate = new ProcessedLog([
            'date_processed' => date('Y-m-d H:i:s'),
            'last_log_date_processed' => $lastLogDate,
        ]);

        $processedDate->save();
    }

    /**
     * @param string $name
     * 
     * @return Carbon
     */
    protected function extractDate(string $name)
    {
        $withoutExt = explode('.', $name);
        $channelAndDate = explode('-', $withoutExt[0], 2);

        return new Carbon($channelAndDate[1]);
    }

    /**
     * @param string[] $files
     *
     * @return Carbon|static
     */
    protected function getLastLogDate(array $files)
    {
        $lastDate = Carbon::create(1970, 1, 1);

        foreach ($files as $file) {
            try {
                $date = $this->extractDate($file);

                if ($date->gt($lastDate)) {
                    $lastDate = $date;
                }
            } catch (\Exception $e) {}
        }

        return $lastDate;
    }

    /**
     * @param string[] $logFiles
     *
     * @return $this
     */
    protected function processLogFiles(array $logFiles)
    {
        $lastProcessedDate = ProcessedLog::orderBy('date_processed', 'desc')->first();
        if ($lastProcessedDate) {
            $lastProcessedDate = (new Carbon($lastProcessedDate->date_processed))->subDay();
        } else {
            $lastProcessedDate = new Carbon('1979-01-01');
        }

        foreach ($logFiles as $logFile) {
            try {
                $logDate = $this->extractDate($logFile);
            } catch (\Exception $e) {
                continue;
            }
            if ($logDate->lte($lastProcessedDate)) {
                continue;
            }

            $parts = explode('.', $logFile);

            if ($parts[1] !== 'log') {
                continue;
            }

            $content = file_get_contents($this->logPath.'/'.$logFile);

            preg_match_all('/((https|http):\/\/[\S]*)/', $content, $matches);

            if (count($matches[0]) > 0) {
                try {
                    $this->extractAndSaveImageUrls($matches[0]);
                } catch (\Exception $e) {}

            }
        }

        return $this;
    }

    /**
     * @param string[] $urls
     *
     * @return $this
     */
    protected function extractAndSaveImageUrls(array $urls)
    {
        foreach ($urls as $url) {
            $size = getimagesize($url);

            if (is_array($size)) {
                $imageUrl = CollectedImageUrl::where('url', '=', $url)->first();

                if ($imageUrl === null) {
                    $collectedImage = new CollectedImageUrl([
                        'url' => $url,
                    ]);

                    $collectedImage->save();
                }
            }
        }

        return $this;
    }
}
