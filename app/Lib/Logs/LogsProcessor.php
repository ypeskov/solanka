<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 17.10.17
 * Time: 18:24
 */

namespace App\Lib\Logs;


use Carbon\Carbon;

class LogsProcessor
{
    protected $channels = [];
    protected $logFiles = [];

    public function setLogInfo()
    {
        $logFiles = scandir(config('config.log_path'));

        foreach ($logFiles as $logFile) {
            if (strpos($logFile, '#') === 0) {
                $channel = substr(explode('-', $logFile, 2)[0], 1);
                if (!in_array($channel, $this->channels)) {
                    $this->channels[] = $channel;
                }

                $this->logFiles[$channel][] = substr($logFile, 1);
            }
        }

        return $this;
    }

    public function getChannels()
    {
        return $this->channels;
    }

    public function getLogFiles()
    {
        return $this->logFiles;
    }

    public function getLogContent($fileName)
    {
        $fileName = '#' . $fileName;

        $fullName = config('config.log_path').'/'.$fileName;

        $file = implode('<br>', file($fullName));

        return $file;
    }

    /**
     * @param string $subject
     * @return array
     */
    public function getSearchResults(string $subject, $from, $till)
    {
        if ($from) {
            $from = $this->prepareDate($from);
        } else {
            $from = $this->prepareDate('1970-01-01');
        }

        if ($till) {
            $till = $this->prepareDate($till);
        } else {
            $till = $this->prepareDate(date('Y-m-d'));
        }

        $results = $this->getParsedResults($from, $till, $subject);

        return implode('<br>', $results);
    }

    protected function getParsedResults($from, $till, $subject)
    {
        $path = config('config.log_path');

        $files = scandir($path);

        $results = [];
        foreach($files as $file) {
            if (stripos($file, '.') === 0) {
                continue;
            }

            $channel = explode('-', $file, 2)[0];
            $date = new Carbon(explode('-', explode('.', $file)[0], 2)[1]); //#autoua-2017-10-09.log

            if ($date->lt($from)) {
                continue;
            }

            if ($date->gt($till)) {
                continue;
            }


            if ( strpos($file, '.') !== 0) {
                $lines = file($path.'/'.$file);

                foreach ($lines as $line) {
                    $position = mb_stripos(mb_strtolower($line), mb_strtolower($subject));

                    if ($position !== false) {
                        $results[] = $this->constructFoundLine($subject, $position, $line, $channel);
                    }
                }
            }
        }

        return $results;
    }

    /**
     * @param string $date
     * @return Carbon
     */
    protected function prepareDate(string $date)
    {
        return new Carbon($date);
    }

    protected function constructFoundLine($subject, $position, $line, $channel)
    {
        $subjectLen = mb_strlen($subject);

        $part1 = mb_substr($line, 0, $position);
        $part2 = mb_substr($line, $position, $subjectLen);
        $part3 = mb_substr($line, $position + $subjectLen);

        $foundLine = '<strong>'.$channel.'</strong>:&nbsp;'.$part1 . '<span class="found-subject">' . $part2 . '</span>' . $part3;

        return $foundLine;
    }
}