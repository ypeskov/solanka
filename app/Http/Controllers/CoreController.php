<?php

namespace App\Http\Controllers;

use App\Models\CollectedImageUrl;

class CoreController extends Controller
{
    const IMAGES_PER_PAGE = 20;

    public function index()
    {
        $images = CollectedImageUrl::orderBy('id', 'desc')->paginate(self::IMAGES_PER_PAGE);

        return view('pages.main', ['images' => $images]);
    }
}
