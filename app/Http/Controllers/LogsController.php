<?php

namespace App\Http\Controllers;

use App\Lib\Logs\LogsProcessor;

class LogsController extends Controller
{
    protected $logProcessor;

    protected $viewData = [];

    public function __construct(LogsProcessor $processor)
    {
        $this->logProcessor = $processor;

        $this->init();
    }

    protected function init()
    {
        $this->logProcessor->setLogInfo();

        $logFiles = $this->logProcessor->getLogFiles();
        foreach ($logFiles as $channel => $fileCollection) {
            rsort($logFiles[$channel]);
        }

        $this->viewData = [
            'channels' => $this->logProcessor->getChannels(),
            'logFiles' => $logFiles,
        ];

    }

    public function index()
    {
        return view('logs.index', $this->viewData);
    }

    public function channelLogs()
    {
        $this->viewData['channel'] = request('channel', null);
        return view('logs.channel', $this->viewData);
    }

    public function showLogFile()
    {
        $this->viewData['logContent'] = $this->logProcessor->getLogContent(\request('file'));

        return view('logs.content', $this->viewData);
    }

    public function searchResult()
    {
        $subject = trim(request('subject', null));
        $from = request('from', null);
        $till = request('till', null);

        $this->viewData['from'] = $from;
        $this->viewData['till'] = $till;

        if ($subject) {
            $this->viewData['subject'] = $subject;

            $this->viewData['searchResults'] = $this->logProcessor->getSearchResults($subject, $from, $till);
        } else {
            $this->viewData['subject'] = '';
            $this->viewData['searchResults'] = '';
        }

        return view('logs.search-results', $this->viewData);
    }
}
