<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CoreController@index')->name('images');

Route::get('/logs', 'LogsController@index')->name('logs');
Route::get('/logs/search-result', 'LogsController@searchResult')->name('searchInLogsResult');
Route::get('/logs/{channel}', 'LogsController@channelLogs')->name('channelLogs');
Route::get('/log/show/{file}', 'LogsController@showLogFile')->name('showLog');
