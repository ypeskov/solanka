<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class AlterTodoTableAddDOne extends Migration
{
    const TABLE_NAME = 'todos';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function(Blueprint $table) {
            $table->boolean('done')->after('author');
            $table->dateTime('when_done')->after('done')->nullable();
            $table->string('who_done')->after('when_done');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function(Blueprint $table) {
            $table->dropColumn('done');
            $table->dropColumn('when_done');
            $table->dropColumn('who_done');
        });
    }
}
