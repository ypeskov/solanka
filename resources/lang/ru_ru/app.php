<?php

return [
    'solanka_ot_bota' => 'Солянка от бота',
    'koleso istorii' => 'Колесо истории',
    'footer' => '#autoua на RusNet рулит',
    'images' => 'картинки',
    'logs' => 'логи',
    'i have logs for next channels' => 'У меня имеется информация о логах со следующих каналов',
    'no channels' => 'Похоже у меня нет информации о каналах',
    'select log date' => 'Выберите дату лога',
    'log content' => 'Содержимое лога',
    'search in logs' => 'искать в логах',
    'search results' => 'результаты поиска',
    'from' => 'с',
    'till' => 'по',
    'search' => 'поиск',

];