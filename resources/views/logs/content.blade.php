@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{__('app.log content')}}
                </div>

                <div class="panel-body">
                    <span v-pre>{!!$logContent!!}</span>
                </div>
            </div>
        </div>
    </div>
@endsection