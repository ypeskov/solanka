@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{__('app.i have logs for next channels')}}
                </div>

                <div class="panel-body">
                    <ul class="list-group">
                        @forelse($channels as $channel)
                            <a href="{{route('channelLogs', $channel)}}" class="btn btn-default">
                                <li class="list-group-item">#{{ $channel }}</li>
                            </a>
                        @empty
                            <li class="list-group-item">{{__('app.no channels')}}</li>
                        @endforelse
                    </ul>
                </div>
            </div>


        </div>
    </div>
@endsection