@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{__('app.select log date')}}
                </div>

                <div class="panel-body">
                    <ul class="list-unstyled">
                        @foreach($logFiles[$channel] as $log)
                            <li>
                                <a href="{{route('showLog', $log)}}">{{$log}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection