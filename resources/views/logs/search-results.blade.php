@extends('layouts.main', ['from' => $from, 'till' => $till, 'subject' => $subject])

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{__('app.search results')}} for "{{$subject}}"
                </div>

                <div class="panel-body">
                    <span v-pre>{!! $searchResults !!}</span>
                </div>
            </div>
        </div>
    </div>
@endsection