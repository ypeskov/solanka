<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{__('app.solanka_ot_bota')}}</title>
    <link href="/css/app.css" rel="stylesheet" />
</head>

<body>
<div id="app" class="container">
    <div class="row">
        <div class="col-xs-12">
            <header>
                <h1>{{__('app.koleso istorii')}}</h1>
            </header>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li @if (route('images') == url()->current()) class="active" @endif>
                                <a href="{{route('images')}}">{{ __('app.images') }} <span class="sr-only">(current)</span></a>
                            </li>

                            <li @if (route('logs') == url()->current() or route('channelLogs', $channel) == url()->current()) class="active" @endif>
                                <a href="{{route('logs')}}">{{ __('app.logs') }}</a>
                            </li>
                        </ul>

                        <search route="{{route('searchInLogsResult')}}"
                                text="@if (isset($subject)){{$subject}}@else @endif"
                                placehold="{{__('app.search')}}"
                                from="@if (isset($from)){{$from}}@else @endif"
                                button-label="{{__('app.search in logs')}}"
                                from-label="{{__('app.from')}}"
                                till-label="{{__('app.till')}}"
                                till="@if (isset($till)){{$till}}@else''@endif">
                        </search>

                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            @yield('content')
        </div>
    </div>

    <div class="row footer">
        <div class="col-xs-12">
            <footer>{{ __('app.footer') }}</footer>
        </div>
    </div>
</div>

<script src="/js/app.js"></script>
</body>
</html>