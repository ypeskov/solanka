@extends('layouts.main')

@section('content')
<div class="images-container">
    <div class="page-links">
        {{ $images->links() }}
    </div>

    @foreach($images as $image)
        <div class="image-item">
            <img src="{{$image->url}}"
                 class="image"
                 alt="{{__('app.kartingo')}}" />
        </div>
    @endforeach

    <div class="page-links">
        {{ $images->links() }}
    </div>
</div>
@endsection