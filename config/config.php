<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 14.10.17
 * Time: 8:23
 */

return [
    'log_path' => env('LOG_PATH', '/home/ypeskov/Programming/krokobot/channels-logs/'),
];